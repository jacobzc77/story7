from django.apps import AppConfig


class ProfilgueConfig(AppConfig):
    name = 'profilgue'
