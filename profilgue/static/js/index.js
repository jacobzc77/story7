var checkbox = document.querySelector('input[name=theme]');


checkbox.addEventListener('change', function() {
    if(this.checked){
        trans()
        document.documentElement.setAttribute('data-theme', 'dark')
    }else{
        trans()
        document.documentElement.setAttribute('data-theme', 'light')
    }
})
let trans = () => {
    document.documentElement.classList.add('transition');
    window.setTimeout(() => {
        document.documentElement.classList.remove('transition')
    },1000)
}
$( "#jQuery-accordion" ).accordion({
    collapsible:true,
    heightStyle: "content",
    active : false
})

document.querySelectorAll('.accordion__button').forEach(button =>{
    button.addEventListener('click', () => {
        const accordionContent = button.nextElementSibling;

        button.classList.toggle('accordion__button--active');

        if (button.classList.contains('accordion__button--active')){
            accordionContent.style.maxHeight = accordionContent.scrollHeight + 'px';
        } else{
            accordionContent.style.maxHeight = 0;
        }
    });
});